from django.urls import path, include
from rest_framework_simplejwt.views import (TokenObtainPairView, TokenRefreshView)
from technodevicesApp import views
from technodevicesApp.views.productsUserView import ProductsUserView

urlpatterns = [
    path('login/', TokenObtainPairView.as_view()),
    path('refresh/', TokenRefreshView.as_view()),
    path('user/', views.UserCreateView.as_view()),
    path('user/<int:pk>/', views.UserDetailView.as_view()),
    path('categoria/', views.CategoriaCreateView.as_view()),
    path('marca/', views.MarcaCreateView.as_view()),
    path('producto/<int:pk>/create/', views.ProductCreateView.as_view()),
    path('productos/<int:user>/',  views.ProductsUserView.as_view()),
    path('producto/remove/<int:user>/<int:pk>/', views.ProductDeleteView.as_view()),
    path('producto/update/<int:user>/<int:pk>/', views.ProductUpdateView.as_view()),
    path('producto/<int:pk>/', views.ProductDetailView.as_view()),
    path('productos/', views.AllProductsListView.as_view()),
]