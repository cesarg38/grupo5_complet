from django.urls import path, include
from rest_framework_simplejwt.views import (TokenObtainPairView, TokenRefreshView)
from technodevicesApp import views

urlpatterns = [
    path('login/', TokenObtainPairView.as_view()),
    path('refresh/', TokenRefreshView.as_view()),
    path('user/', views.UserCreateView.as_view()),
    path('user/<int:pk>/', views.UserDetailView.as_view()),
    path('categoria/', views.CategoriaCreateView.as_view()),
    path('marca/', views.MarcaCreateView.as_view()),
    path('producto/', views.ProductoCreateView.as_view()),
]