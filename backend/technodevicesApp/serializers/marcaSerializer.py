from django.db.models import fields
from django.db.models.base import Model
from rest_framework import serializers
from technodevicesApp.models.marca import Marca


class MarcaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Marca
        fields = ['nombre']

    def to_representation(self, obj):
        marca = Marca.objects.get(id=obj.id)
        return {
            'nombre': marca.nombre
            }