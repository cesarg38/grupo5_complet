from django.db.models import fields
from django.db.models.base import Model
from rest_framework import serializers
from technodevicesApp.models.producto import Producto

class ProductoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Producto
        fields = ['vendedor','nombre','marca','precio','descripcion','nuevo','categoria','fecha_publicacion','imagen']

    def create(self, validated_data):
        productoData = validated_data
        productInstance = Producto.objects.create(**productoData)
        return productInstance


