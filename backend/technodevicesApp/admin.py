from django.contrib import admin

from .models.account import Account
from .models.user import User
from .models.producto import Producto
from .models.categoria import Categoria
from .models.marca import Marca


admin.site.register(User)
admin.site.register(Account)
admin.site.register(Producto)
admin.site.register(Categoria)
admin.site.register(Marca)

# Register your models here.
