from .userCreateView import UserCreateView
from .userDetailView import UserDetailView
from .productoCreateView import ProductoCreateView
from .marcaCreateView import MarcaCreateView
from .categoriaCreateView import CategoriaCreateView