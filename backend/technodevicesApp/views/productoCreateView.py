from rest_framework import status, views
from rest_framework.response import Response
from technodevicesApp.serializers.productoSerializer import ProductoSerializer


class ProductoCreateView(views.APIView):

    def post(self, request, *args, **kwargs):
        serializer = ProductoSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        return Response(serializer.data, status=status.HTTP_201_CREATED)

    
    def get(self, request, *args):
        return Response()