from rest_framework import generics, status
from rest_framework.response import Response
from technodevicesApp.models.producto import Producto
from technodevicesApp.serializers.productSerializer import ProductSerializer

class ProductDetailView(generics.RetrieveAPIView):
    queryset = Producto.objects.all()
    serializer_class = ProductSerializer

    def get(self, request, pk, *args, **kwargs):
        producto_buscado = Producto.objects.get(pk = pk)
        producto = {
            'título': producto_buscado.titulo,
            'vendedor': producto_buscado.vendedor_id,
            'marca': producto_buscado.marca_id,
            'precio': producto_buscado.precio,
            'descripción': producto_buscado.descripcion,
            'nuevo': producto_buscado.nuevo,
            'categoría': producto_buscado.categoria_id,
            'fecha de publicación': producto_buscado.fecha_publicacion
            }
        return Response(producto, status=status.HTTP_200_OK)