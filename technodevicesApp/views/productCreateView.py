from rest_framework import status, views
from rest_framework.response import Response
from technodevicesApp.serializers.productSerializer import ProductSerializer
from django.conf import settings
from rest_framework_simplejwt.backends import TokenBackend
from rest_framework.permissions import IsAuthenticated
from technodevicesApp.models.user import User
from technodevicesApp.serializers.userSerializer import UserSerializer


class ProductCreateView(views.APIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        token = request.META.get('HTTP_AUTHORIZATION')[7:]
        tokenBackend = TokenBackend(algorithm=settings.SIMPLE_JWT['ALGORITHM'])
        valid_data = tokenBackend.decode(token,verify=False)
        if valid_data['user_id'] != kwargs['pk']:
            stringResponse = {'detail':'Unauthorized Request'}
            return Response(stringResponse, status=status.HTTP_401_UNAUTHORIZED)
        serializer = ProductSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        print(self.request.user.id)
        serializer.save( vendedor = self.request.user)

        return Response(serializer.data, status=status.HTTP_201_CREATED)