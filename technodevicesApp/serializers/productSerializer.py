from rest_framework import serializers
from technodevicesApp.models.producto import Producto
from technodevicesApp.models.productimages import ProductImages
class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Producto   #modelo que se va a usar
        fields = ['id','titulo','marca','precio','descripcion','nuevo','categoria']  #claves que se quieren usar, adicional la cuenta, para conocer los datos de la cuenta asosciada a ese usario
 
    def create(self, validated_data): #validated_data: datos que ya han sido previamente validados,
        productInstance = Producto.objects.create(**validated_data) # crea el usuario con las claves seleccionadas sin la clave account y le asigna el id de ese usuario creado a la variable
        ProductImages.objects.create(producto=productInstance) # crea la cuenta con el id del usuario creado y los datos de la cuenta
        return productInstance

    def to_representation(self, obj):
        producto = Producto.objects.get(id=obj.id)
        return {
            'título': producto.titulo,
            'vendedor': producto.vendedor_id,
            'marca': producto.marca_id,
            'precio': producto.precio,
            'descripción': producto.descripcion,
            'nuevo': producto.nuevo,
            'categoría': producto.categoria_id,
            'fecha de publicación': producto.fecha_publicacion
            }

