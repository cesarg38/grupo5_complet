from rest_framework import serializers
from technodevicesApp.models.categoria import Categoria


class CategoriaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Categoria
        fields = ['nombre']

    def to_representation(self, obj):
        categoria = Categoria.objects.get(id=obj.id)
        return {
            'nombre': categoria.nombre
            }