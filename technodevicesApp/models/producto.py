from django.db import models
from .user import User
from .categoria import Categoria
from .marca import Marca
from datetime import datetime

class Producto(models.Model):
    id = models.AutoField(primary_key=True)
    vendedor = models.ForeignKey(User, related_name='vendedor', on_delete=models.CASCADE)
    titulo = models.CharField('Nombre', max_length= 50)
    marca = models.ForeignKey(Marca, on_delete= models.PROTECT)
    precio = models.IntegerField()
    descripcion = models.TextField()
    nuevo = models.BooleanField()
    categoria = models.ForeignKey(Categoria, on_delete= models.PROTECT)
    fecha_publicacion = models.DateField('Fecha publicación', default= datetime.now)
    


